package com.example.actividad2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QuadraticEquationTest {
    @Test public void solutionTest() throws Exception {
        QuadraticEquation equation1 = new QuadraticEquation(-1, 3, 4);
        QuadraticEquation equation2 = new QuadraticEquation(1, 2, 1);
        QuadraticEquation equation3 = new QuadraticEquation(5, 5, 66);


        assertEquals ("Las dos soluciones son reales: x1 = -4.0 y x2 = 1.0", equation1.printSolution());
        assertEquals ("La equación solo tiene un resultado: x1 = -1.0", equation2.printSolution());
        assertEquals ("La equación no tiene soluciones reales", equation3.printSolution());
    }

    @Test (expected = Exception.class)
    public void ExceptionTest() throws Exception {
        QuadraticEquation eq1 = new QuadraticEquation(3, 2, 4);
        QuadraticEquation eq2 = new QuadraticEquation(0, 2, 1);
    }




}
