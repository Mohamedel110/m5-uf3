package com.example.actividad2;

public class QuadraticEquation {
    private double a, b, c;
    private double[] solution;

    public QuadraticEquation(double a, double b, double c) throws Exception {
        this.solution = new double[2];
        if (a == 0) {
            throw new Exception("El atributo a no puede ser 0");
        }
        this.a = a;
        this.b = b;
        this.c = c;

    }

    public String printSolution() {
        String result = "";
        final double calc = (b * b) - (4 * a * c);
        if (calc > 0) {
            solution[0] = (b + Math.sqrt((b * b) - (4 * a * c))) / (2 * a);
            solution[1] = (b - Math.sqrt((b * b) - (4 * a * c))) / (2 * a);
            result = "Las dos soluciones son reales: x1 = " + solution[0] + " y x2 = " + solution[1];
        } else if (calc == 0) {
            result = "La equación solo tiene un resultado: x1 = " + (-b) / (2 * a);
        } else if (calc < 0) {
            result = "La equación no tiene soluciones reales";
        }
        return result;
    }


}
