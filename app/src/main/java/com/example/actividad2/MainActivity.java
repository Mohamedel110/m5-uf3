package com.example.actividad2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText a, b, c;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        a = findViewById(R.id.a);
        b = findViewById(R.id.b);
        c = findViewById(R.id.c);

        Button calculate;
        calculate = findViewById(R.id.calculate);
        calculate.setOnClickListener(this);
        result = findViewById(R.id.result);
    }

    @Override
    public void onClick(View v) {
        String textA = a.getText().toString();
        String textB = b.getText().toString();
        String textC = c.getText().toString();
        if (textA.isEmpty() || textB.isEmpty() || textC.isEmpty()) {
            Toast toast =
                    Toast.makeText(getApplicationContext(), "Faltan datos", Toast.LENGTH_LONG);
            toast.show();
        } else {
            double numberA = Double.parseDouble(textA);
            double numberB = Double.parseDouble(textB);
            double numberC = Double.parseDouble(textC);
            try {
                QuadraticEquation quadraticEquation = new QuadraticEquation(numberA, numberB, numberC);
                result.setText(quadraticEquation.printSolution());
            } catch (Exception e) {
                Toast toast =
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }
}
