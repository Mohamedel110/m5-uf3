package com.example.actividad2;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSubstring;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class, true,
                    true);

    @Test
    public void exceptionTest() {
        onView(withId(R.id.a)).perform(typeText("0"));
        onView(withId(R.id.b)).perform(typeText("-10"));
        onView(withId(R.id.c)).perform(typeText("20"));
        onView(withId(R.id.calculate)).perform(click());
        onView(withText("El atributo a no puede ser 0")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void aErrorTest() {
        onView(withId(R.id.a)).perform(typeText(""));
        onView(withId(R.id.b)).perform(typeText("10"));
        onView(withId(R.id.c)).perform(typeText("3"));
        onView(withId(R.id.calculate)).perform(click());
        onView(withText("Faltan datos")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void bErrorTest() {
        onView(withId(R.id.a)).perform(typeText("8"));
        onView(withId(R.id.b)).perform(typeText(""));
        onView(withId(R.id.c)).perform(typeText("-22"));
        onView(withId(R.id.calculate)).perform(click());
        onView(withText("Faltan datos")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void cErrorTest() {
        onView(withId(R.id.a)).perform(typeText("-9"));
        onView(withId(R.id.b)).perform(typeText("4"));
        onView(withId(R.id.c)).perform(typeText(""));
        onView(withId(R.id.calculate)).perform(click());
        onView(withText("Faltan datos")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void SuccessTest() {

        onView(withId(R.id.a)).perform(replaceText("2"));
        onView(withId(R.id.b)).perform(replaceText("5"));
        onView(withId(R.id.c)).perform(replaceText("6"));
        onView(withId(R.id.calculate)).perform(click());
        onView(withId(R.id.result)).check(matches(withSubstring("La equación no tiene soluciones reales")));
    }
}
